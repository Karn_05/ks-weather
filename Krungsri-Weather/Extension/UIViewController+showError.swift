import UIKit

extension UIViewController {
    
    func showError(errorMessage: String, submitButtonAction: (() -> Void)? = nil){
        let alert = UIAlertController(title: "Error",
                                      message: errorMessage,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            submitButtonAction?()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
}
