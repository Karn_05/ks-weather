import UIKit

extension Date {
    func toString(withFormat format: String = "EEEE, d MMM yyyy-hh:mm a") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = format
        let str = dateFormatter.string(from: self)
        return str
    }
}
