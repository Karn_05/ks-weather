/* 
Copyright (c) 2022 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct ForecastList : Mappable {
	var dt : Int?
	var main : ForecastMain?
	var weather : [ForecastWeather]?
	var clouds : ForecastClouds?
	var wind : ForecastWind?
	var visibility : Int?
	var pop : Double?
	var sys : ForecastSys?
	var dt_txt : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		dt <- map["dt"]
		main <- map["main"]
		weather <- map["weather"]
		clouds <- map["clouds"]
		wind <- map["wind"]
		visibility <- map["visibility"]
		pop <- map["pop"]
		sys <- map["sys"]
		dt_txt <- map["dt_txt"]
	}

}
