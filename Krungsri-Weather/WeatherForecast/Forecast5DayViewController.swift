//
//  Forecast5DayViewController.swift
//  Krungsri-Weather
//
//  Created by Kittithat Pintaku on 30/10/2565 BE.
//  Copyright (c) 2565 BE ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol Forecast5DayDisplayLogic: AnyObject
{
    func successFetchedForecast(viewModel: Forecast5Day.FetchWeather.ViewModel)
    func errorFetchedForecast(viewModel: Forecast5Day.FetchWeather.ViewModel)
}

class Forecast5DayViewController: UIViewController, Forecast5DayDisplayLogic
{
    @IBOutlet weak var tableViewForecast: UITableView!
    
    var interactor: Forecast5DayBusinessLogic?
    var router: (NSObjectProtocol & Forecast5DayRoutingLogic & Forecast5DayDataPassing)?
    var dataForecast: (sectionDate: [String], tempUnit: String?, forecastArray: [[ForecastList]])?
    
    // MARK: Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
    {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup()
    {
        let viewController = self
        let interactor = Forecast5DayInteractor()
        let presenter = Forecast5DayPresenter()
        let router = Forecast5DayRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: Routing
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let scene = segue.identifier {
            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
            if let router = router, router.responds(to: selector) {
                router.perform(selector, with: segue)
            }
        }
    }
    
    // MARK: View lifecycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tableViewForecast.register(UINib.init(nibName: "ForecastTableViewCell", bundle: nil), forCellReuseIdentifier: "forecastCell")
        fetchedForecastWeather()
        fetchedDataStore()
    }
    
    // MARK: Do something
    
    func fetchedDataStore(){
        self.title = interactor?.fetchCityName()
    }
    
    func fetchedForecastWeather()
    {
        let request = Forecast5Day.FetchWeather.Request()
        interactor?.fetchForecastWeather(request: request)
    }
    
    func successFetchedForecast(viewModel: Forecast5Day.FetchWeather.ViewModel) {
        self.dataForecast = viewModel.filterForecastFromDate()
        self.tableViewForecast.reloadData()
    }
    
    func errorFetchedForecast(viewModel: Forecast5Day.FetchWeather.ViewModel) {
        showError(errorMessage: viewModel.errorMessage ?? "") {
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension Forecast5DayViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let cou = self.dataForecast?.forecastArray.count else {
            return 0
        }
        return cou
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let cou = self.dataForecast?.forecastArray[section].count else {
            return 0
        }
        return cou
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "forecastCell", for: indexPath) as! ForecastTableViewCell
        cell.configure(data: self.dataForecast?.forecastArray[indexPath.section][indexPath.row],
                       tempUnit: self.dataForecast?.tempUnit)
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.dataForecast?.sectionDate[section].toDate(withFormat: "yyyy-MM-d")?.toString(withFormat: "EEEE, d MMM yyyy")
        
    }
    
}
