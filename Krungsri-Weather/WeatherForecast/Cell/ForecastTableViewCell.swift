//
//  ForecastTableViewCell.swift
//  Krungsri-Weather
//
//  Created by Kittithat Pintaku on 30/10/2565 BE.
//

import UIKit
import Kingfisher

class ForecastTableViewCell: UITableViewCell {

    @IBOutlet weak var lbTime: UILabel!
    @IBOutlet weak var imgWeather: UIImageView!
    @IBOutlet weak var lbTemp: UILabel!
    @IBOutlet weak var lbHumidity: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(data: ForecastList?, tempUnit: String?){
        let unit = tempUnit == "metric" ? "°C" : "°F"
        self.lbTime.text = data?.dt_txt?.toDate()?.toString(withFormat: "hh:mm a")
        self.imgWeather.kf.setImage(with: URL(string: "https://openweathermap.org/img/w/\(data?.weather?[0].icon ?? "").png"))
        self.lbTemp.text = "\(data?.main?.temp ?? 0) \(unit)"
        self.lbHumidity.text = "\(data?.main?.humidity ?? 0)  %"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
