//
//  Forecast5DayModels.swift
//  Krungsri-Weather
//
//  Created by Kittithat Pintaku on 30/10/2565 BE.
//  Copyright (c) 2565 BE ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

enum Forecast5Day
{
    // MARK: Use cases
    
    enum FetchWeather
    {
        struct Request
        {
            var cityName: String = ""
            var tempUnit: String = "metric"
        }
        struct Response
        {
            var forecast : Forecast5DayModel?
            var isError: Bool
            var errorMessage: String?
            var tempUnit: String?
        }
        
        struct FilterForecast
        {
            var forecast : [ForecastList]?
        }
        
        struct ViewModel
        {
            var list : [ForecastList]?
            var isError: Bool
            var errorMessage: String?
            var tempUnit: String?
            
            func filterForecastFromDate() -> (sectionDate: [String],
                                              tempUnit: String?,
                                              forecastArray: [[ForecastList]]) {
                var castDate = [String]()
                list?.forEach{ castDate.append(String(( $0.dt_txt?.split(separator: " ")[0] ) ?? "")) }
                let uniqueDate = Array(Set(castDate)).sorted()
                var forecastNewArray = [[ForecastList]]()
                
                uniqueDate.enumerated().forEach { itt, checkDate in
                    forecastNewArray.append([])
                    list?.forEach({ forecast in
                        let checkDup = String(( forecast.dt_txt?.split(separator: " ")[0] ) ?? "")
                        if checkDup == checkDate {
                            forecastNewArray[itt].append(forecast)
                        }
                    })
                }
                return (uniqueDate, tempUnit, forecastNewArray)
            }
        }
    }
}


