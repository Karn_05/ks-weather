# KS-Weather

## Getting Started

### [Cocoapods](https://github.com/CocoaPods/CocoaPods) (The dependency manager)

Cocoapods is an dependency manager for Swift and Objective-C.
A lot of dependencies manager in the world but most open-source supported via Cocoapods.

*To install cocoapods you need broadband internet.*

```bash
pod install

# after pod install pod will create file `Krungsri-Weather.xcworkspace`
# so open `Krungsri-Weather.xcworkspace` instead of Krungsri-Weather.xcodeproj
open "Krungsri-Weather.xcworkspace"
```

### User Manual

*****Weather page*****

- Can fill name city in English & Thai
- Display temperature for today
- Display humidity for today
- Can switch temperature between Celsius and Fahrenheit


*****List page*****
- Can see 5-day forecast

*****Pattern*****
- Swift language
- Design Patten with Clean Architecture
- Auto Layout
